from pathlib import Path

from local_config import API_KEY_FIXER, MAIL_RECEIVER, SMS_RECEIVER

# Fixer API
BASE_URL = "http://data.fixer.io/api/latest?access_key="
API_KEY = API_KEY_FIXER

URL = BASE_URL + API_KEY

# ROOT Path Project
BASE_DIR = Path(__file__).resolve().parent

# Rules
rules = {
    "archive": {
        "enable": True,
    },
    "mail": {
        "enable": True,
        "sender": "finance@inprobes.com",
        "receiver": MAIL_RECEIVER,
        # preferred default is empty list.
        "preferred": [
            "BTC",
            "IRR",
            "CAD",
            "USD",
            "AED",
        ],
    },
    "notification": {
        "enable": True,
        "receiver": SMS_RECEIVER,
        # preferred default is empty dict.
        "preferred": {
            "BTC": {
                "min": 0.000046,
                "max": 0.000052,
            },
            "IRR": {
                "min": 30000,
                "max": 50000,
            },
            "CAD": {
                "min": 1.25,
                "max": 1.35,
            },
        },
    },
}
