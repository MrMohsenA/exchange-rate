import json
from datetime import datetime
from pathlib import Path

import requests
from khayyam import JalaliDatetime

from requests.exceptions import HTTPError, ConnectionError

from config import BASE_DIR, rules
from mail import send_smtp_mail
from notification import send_sms


def get_data(url):
    """
    send the request to the desired api and convert json to object's python
    :param url: str
    :return: dict or None
    """
    try:
        response = requests.get(url)

        if response.ok:
            return json.loads(response.text)

    except HTTPError as http_error:
        print(f"HTTPError: {http_error}")
    except ConnectionError as conn_error:
        print(f"ConnectionError: {conn_error}")


def archive(timestamp, rates):
    """
    check archives dir exists or not. if it doesn't exist create new dir in path.
    archive rates in files
    :param timestamp: int
    :param rates: dict
    :return: None
    """
    Path(f"{BASE_DIR}/archives").mkdir(parents=True, exist_ok=True)

    with open(file=f"archives/{timestamp}.json", mode="w") as file_handler:
        file_handler.write(json.dumps(rates))


def get_preferred_rates(rates):
    """
    check preferred rates is exists or not.
    if it does exist create preferred_rates contains pair "rate_name": "rate_value"
        and return it.
    if it doesn't exist return all available rates.
    :param rates: dict
    :return: dict
    """
    preferred = rules["mail"]["preferred"]
    if preferred:
        preferred_rates = {rate: rates.get(rate) for rate in preferred}

        rates = preferred_rates

    return rates


def send_mail(timestamp, rates):
    """
    get timestamp and rates, check if there is preferred rates and
    then send email through smtp protocol.
    :param timestamp: int
    :param rates: dict
    :return: None
    """
    subject = f"Subject: Fixer API"

    preferred_rates = get_preferred_rates(rates)

    now = datetime.now().strftime("%Y-%b-%d  %a  %H:%M")
    jalali_now = JalaliDatetime.now().strftime("%Y-%G-%d  %E  %H:%M")

    message = f"Timestamp: {timestamp}\nDatetime: {now}\nJalali Datetime: {jalali_now}\n\n"
    for rate, value in preferred_rates.items():
        message += f"The price of the {rate} rate against the euro rate is {value}\n\n"

    body = message
    send_smtp_mail(subject, body)


def check_notify(rates):
    """
    get preferred rates from rules and check current price rate reached than
    lowest or highest specified price in rules.
    :param rates:
    :return:
    """
    report = ""

    preferred = rules["notification"]["preferred"]

    for rate in preferred.keys():
        current_price_rate = rates.get(rate)

        lowest_price_set_rate = preferred[rate]["min"]
        if current_price_rate <= lowest_price_set_rate:
            report += f'{rate} reached min : {current_price_rate} \n'

        highest_price_set_rate = preferred[rate]["max"]
        if current_price_rate >= highest_price_set_rate:
            report += f'{rate} reached max : {current_price_rate} \n'

    return report


def send_notification(timestamp, report):
    """
    send sms notification
    :param timestamp:
    :param report:
    :return:
    """
    now = datetime.now().strftime("%Y-%b-%d  %a  %H:%M")
    jalali_now = JalaliDatetime.now().strftime("%Y-%G-%d  %E  %H:%M")

    msg = f"Timestamp: {timestamp}\nDatetime: {now}\nJalali Datetime: {jalali_now}\n\n{report}"
    send_sms(msg)
