from config import URL
from exchange import (
    get_data,
    archive,
    send_mail,
    send_notification,
    check_notify,
)
from config import rules


def run():
    data = get_data(URL)

    timestamp = data.get("timestamp")
    rates = data.get("rates")

    if rules["archive"]["enable"]:
        archive(timestamp=timestamp, rates=rates)

    if rules["mail"]["enable"]:
        send_mail(timestamp=timestamp, rates=rates)

    if rules["notification"]["enable"]:
        report = check_notify(rates=rates)
        if report:
            send_notification(timestamp=timestamp, report=report)


if __name__ == '__main__':
    run()
