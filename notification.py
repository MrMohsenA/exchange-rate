from kavenegar import KavenegarAPI, APIException, HTTPException

from config import rules
from local_config import API_KEY_KAVENEGAR


def send_sms(msg):
    try:
        api = KavenegarAPI(API_KEY_KAVENEGAR)
        params = {
            'sender': '',
            'receptor': rules["notification"]["receiver"],
            'message': msg,
        }
        response = api.sms_send(params)
        print(response)
    except APIException as api_exception:
        print(api_exception)
    except HTTPException as http_exception:
        print(http_exception)
