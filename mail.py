import smtplib

from email.mime.text import MIMEText

from config import rules
from local_config import USER, PASS


def send_smtp_mail(subject, body):
    msg = MIMEText(body)
    msg["Subject"] = subject
    msg["From"] = rules["mail"]["sender"]
    msg["To"] = rules["mail"]["receiver"]

    with smtplib.SMTP("smtp.mailtrap.io", 2525) as server:
        server.login(USER, PASS)
        server.sendmail(msg["From"], msg["To"], msg.as_string())
